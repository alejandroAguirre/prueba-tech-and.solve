
$(document).ready(function(){
    
    /**
    *
    * Descripción. Se inicializa el plug in select2 basico sobre todos los inputs
    * con la clase selectBusqueda.
    * @author Johan Alejandro Aguirre Escobar
    */
    $('.selectBusqueda').select2({'width': '100%'});
    
    function listarVuelos() 
    {
        $.ajax({
            type: 'POST',
            url: "listarVuelos.html",
            beforeSend: function () {
            },
            data: {
                ciudad_origen: $('#registrarReserva').find('select[name="ciudad_origen"]').val(),
                ciudad_destino: $('#registrarReserva').find('select[name="ciudad_destino"]').val()
            },
            success: function (data) {
                console.log(data);
                alert("exito");
            },
            error: function () {
                alert("error");
            },
            complete: function () {
            }
        });
    }
    
});
