<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="public/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="public/css/animate.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">
            <%@include file="template/side_menu.jsp" %>
            <div id="page-wrapper" class="gray-bg">
                <%@include file="template/header.jsp" %>

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Home</h2>
                        <ol class="breadcrumb">
                            <li class="active">
                                <strong>Home</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Home</h5>
                                    <div class="ibox-tools">
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    
                                    <h1>Bienvenido</h1>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%@include file="template/footer.jsp" %>
            </div>
        </div>
        
        <!-- jquery-3.2.1 -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
