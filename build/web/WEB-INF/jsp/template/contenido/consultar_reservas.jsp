
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultar reservas</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="public/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">
        
        <link href="<c:url value="public/plugins/select2/select2.min.css"/>" rel="stylesheet">
        <link href="<c:url value="public/plugins/sweealert/sweealert.css"/>" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">
            <%@include file="../side_menu.jsp" %>
            <div id="page-wrapper" class="gray-bg">
                <%@include file="../header.jsp" %>

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Consultar reservas</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.htm">Home</a>
                            </li>
                            <li class="active">
                                <strong>Consultar reservas</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Consulte Las reservas de un usuario</h5>
                                    <div class="ibox-tools">
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="col-md-12">
                                        <form role="form" class="form-inline" action="consultarReservasUsuario.htm" method="POST">
                                            <div class="form-group">
                                                <label for="nro_documento" class="sr-only">N�mero de documento</label>
                                                <input type="text" class="form-control" name="nro_documento" maxlength="11">
                                            </div>
                                            <button class="btn btn-white" type="submit">Buscar</button>
                                        </form>
                                    </div>
                                    <div class="col-md-12">
                                        <h5>Detalle</h5>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Nro documento</th>
                                                    <th>Nombre</th>
                                                    <th>Detalle reserva</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${reservasUsuario}" var="reserva">
                                                    <tr>
                                                        <td><c:out value="${reserva.nro_documento}"/></td>
                                                        <td><c:out value="${reserva.nombres} ${reserva.apellidos}"/></td>
                                                        <td>
                                                            <label>Reserva Nro:</label> <c:out value="${reserva.id}"/>
                                                            <br>
                                                            <label>Realizada en:</label> <c:out value="${reserva.creado_en}"/>
                                                            <br>
                                                            <label>Origen:</label> <c:out value="${reserva.municipio_origen}"/> -
                                                            <label>Fecha Salida: </label> <c:out value="${reserva.vs_fecha_salida}"/>
                                                            <br>
                                                            <label>Destino:</label> <c:out value="${reserva.municipio_destino}"/> -
                                                            <label>Fecha Llegada: </label> <c:out value="${reserva.vs_fecha_llegada}"/>
                                                            <br>
                                                            <label>Precio:</label> <c:out value="${reserva.vs_precio}"/> (COP)
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>


                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%@include file="../footer.jsp" %>
            </div>
        </div>

        <!-- jquery-3.2.1 -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!-- validate -->
        <script src="<c:url value="/public/plugins/validate/jquery.validate.min.js"/>"></script>
        <script src="<c:url value="/public/plugins/validate/additional-methods.js"/>"></script>
        <script src="<c:url value="/public/plugins/validate/messages_es.js"/>"></script>
        <!-- select2 -->
        <script src="<c:url value="/public/plugins/select2/select2.full.min.js"/>"></script>
        <script src="<c:url value="/public/plugins/select2/es.js"/>"></script>
        <script src="<c:url value="/public/js/app.js"/>"></script>
    </body>
</html>



