
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reservar vuelo</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="public/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="public/css/style.css" rel="stylesheet">

        <link href="<c:url value="public/plugins/select2/select2.min.css"/>" rel="stylesheet">
        <link href="<c:url value="public/plugins/sweealert/sweealert.css"/>" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">
            <%@include file="../side_menu.jsp" %>
            <div id="page-wrapper" class="gray-bg">
                <%@include file="../header.jsp" %>

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Reservar vuelo</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.htm">Home</a>
                            </li>
                            <li class="active">
                                <strong>Reservar vuelo</strong>
                            </li>
                        </ol>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Realice la reserva de un vuelo</h5>
                                    <div class="ibox-tools">
                                    </div>
                                </div>
                                <div class="ibox-content"> 
                                    
                                    <div class="col-lg-12">
                                        <form class="form-horizontal" id="registrarReserva" action="reservarVuelo.htm" method="POST">
                                            <div class="form-group"><label class="col-lg-2 control-label">N�mero de documento</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="nro_documento"> 
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-2 control-label">Nombres</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="nombres"> 
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="apellidos"> 
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-2 control-label">Edad</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="edad"> 
                                                </div>
                                            </div>
                                            <div class="form-group"><label class="col-lg-2 control-label">Seleccione vuelo</label>
                                                <div class="col-lg-10">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Seleccionar</th>
                                                                <th>Ciudad origen</th>
                                                                <th>Ciudad destino</th>
                                                                <th>Fecha de salida</th>
                                                                <th>Fecha de llegada</th>
                                                                <th>Precio por persona (COP)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${vuelos}" var="vuelo">
                                                                <tr>
                                                                    <td>
                                                                        <input type="radio" name="vuelo" value="${vuelo.id}" class="form-control">
                                                                    </td>
                                                                    <td><c:out value="${vuelo.ciudad_origen}"/></td>
                                                                    <td><c:out value="${vuelo.ciudad_destino}"/></td>
                                                                    <td><c:out value="${vuelo.fecha_salida}"/></td>
                                                                    <td><c:out value="${vuelo.fecha_llegada}"/></td>
                                                                    <td><c:out value="${vuelo.precio}"/></td>
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-sm btn-white" type="submit">Reservar</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <%@include file="../footer.jsp" %>
            </div>
        </div>

        <!-- jquery-3.2.1 -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- validate 
        <script src="<c:url value="/public/plugins/validate/jquery.validate.min.js"/>"></script>
        <script src="<c:url value="/public/plugins/validate/additional-methods.js"/>"></script>
        <script src="<c:url value="/public/plugins/validate/messages_es.js"/>"></script>
        -->
        <!-- select2 -->
        <script src="<c:url value="/public/plugins/select2/select2.full.min.js"/>"></script>
        <script src="<c:url value="/public/plugins/select2/es.js"/>"></script>
        <script src="<c:url value="/public/js/app.js"/>"></script>
    </body>
</html>



