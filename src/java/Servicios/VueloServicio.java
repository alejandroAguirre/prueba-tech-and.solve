/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Conexion.Conexion;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Alejandro
 */
public class VueloServicio {
    
    private JdbcTemplate jdbcTemplate;
    
    public VueloServicio() 
    {
        Conexion con = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(con.conectar());
    }
    
    public List consultarVuelos()
    { 
        String sql = "SELECT v.id, mo.municipio as ciudad_origen, md.municipio as ciudad_destino, v.precio, DATE_FORMAT(v.fecha_salida, '%d-%c-%Y %H:%i') as fecha_salida, DATE_FORMAT(v.fecha_llegada, '%d-%c-%Y %H:%i') as fecha_llegada"
                    + " FROM vuelos v, municipios mo, municipios md"
                    + " WHERE v.ciudad_origen = mo.id_municipio"
                    + " AND v.ciudad_destino = md.id_municipio"
                    + " ORDER BY mo.municipio asc, md.municipio asc";
                
        List vuelos = this.jdbcTemplate.queryForList(sql);
        return vuelos;
    }
    
    public List filtrarVuelos(String ciudadOrigen, String ciudadDestino)
    {
        String sql = "SELECT mo.municipio as ciudad_origen, md.municipio as ciudad_destino, v.precio"
                    + " FROM vuelos v, municipios mo, municipios md"
                    + " WHERE v.ciudad_origen = mo.id_municipio"
                    + " AND v.ciudad_destino = md.id_municipio"
                    + " AND v.ciudad_origen = "+ciudadOrigen
                    + " AND v.ciudad_destino = "+ciudadDestino
                    + " ORDER BY mo.municipio asc, md.municipio asc";
                
        List vuelos = this.jdbcTemplate.queryForList(sql);
        return vuelos;
    }
    
}
