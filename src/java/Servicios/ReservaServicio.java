/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Conexion.Conexion;
import Models.Reservacion;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Alejandro
 */
public class ReservaServicio {
    
    private JdbcTemplate jdbcTemplate;
    
    public ReservaServicio() 
    {
        Conexion con = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(con.conectar());
    }
    
    public List consultarReservasUsuario(String nroDocumento)
    { 
        String sql = "SELECT u.nro_documento, u.nombres, u.apellidos, r.id, mo.municipio as municipio_origen, md.municipio as municipio_destino, DATE_FORMAT(r.creado_en, '%d-%c-%Y %H:%i') as creado_en, vs.precio as vs_precio, DATE_FORMAT(vs.fecha_salida, '%d-%c-%Y %H:%i') as vs_fecha_salida, DATE_FORMAT(vs.fecha_llegada, '%d-%c-%Y %H:%i') as vs_fecha_llegada " 
                    +"FROM usuario u, vuelos vs, municipios mo, municipios md, reservacion r "
                    +"where u.nro_documento = r.usuario " 
                    +"AND vs.id = r.vuelo_salida "
                    +"AND vs.ciudad_origen = mo.id_municipio "
                    +"AND vs.ciudad_destino = md.id_municipio "
                    +"AND u.nro_documento = '"+ nroDocumento+"' "
                    +"ORDER BY r.creado_en DESC";
                
        List vuelos = this.jdbcTemplate.queryForList(sql);
        return vuelos;
    }
    
    public void registrarReservaUsuario(Reservacion r)
    { 
        String sqlUsuario = "INSERT INTO usuario (nro_documento, nombres, apellidos, edad) "
                    + "values(?,?,?,?)";
                
        this.jdbcTemplate.update(sqlUsuario, r.getUsuario().getNroDocumento(), 
                                r.getUsuario().getNombres(), 
                                r.getUsuario().getApellidos(), 
                                r.getUsuario().getEdad());
        
        
        String sqlReservacion = "INSERT INTO reservacion (vuelo_salida, usuario) "
                    + "values(?,?)";
                
        this.jdbcTemplate.update(sqlReservacion, r.getVueloSalida().getId(), 
                                r.getUsuario().getNroDocumento());
    }
    
}
