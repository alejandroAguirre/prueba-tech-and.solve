/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servicios;

import Conexion.Conexion;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Alejandro
 */
public class CiudadServicio {
    
    private JdbcTemplate jdbcTemplate;
    
    public CiudadServicio() 
    {
        Conexion con = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(con.conectar());
    }
    
    public List listarCiudades()
    { 
        String sql = "SELECT m.id_municipio, m.municipio"
                        + " FROM municipios m"
                        + " ORDER BY m.municipio asc";
                
        List vuelos = this.jdbcTemplate.queryForList(sql);
        return vuelos;
    }
    
}
