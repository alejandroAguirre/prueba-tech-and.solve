/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Alejandro
 */
public class Reservacion {
    
    private Integer id;
    private Vuelo vueloSalida;
    private Vuelo vueloRetorno;
    private Usuario usuario;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the vueloSalida
     */
    public Vuelo getVueloSalida() {
        return vueloSalida;
    }

    /**
     * @param vueloSalida the vueloSalida to set
     */
    public void setVueloSalida(Vuelo vueloSalida) {
        this.vueloSalida = vueloSalida;
    }

    /**
     * @return the vueloRetorno
     */
    public Vuelo getVueloRetorno() {
        return vueloRetorno;
    }

    /**
     * @param vueloRetorno the vueloRetorno to set
     */
    public void setVueloRetorno(Vuelo vueloRetorno) {
        this.vueloRetorno = vueloRetorno;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
    
}
