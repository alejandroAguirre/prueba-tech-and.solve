/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Conexion.Conexion;
import Models.Reservacion;
import Models.Usuario;
import Models.Vuelo;
import Servicios.CiudadServicio;
import Servicios.ReservaServicio;
import Servicios.VueloServicio;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;
import java.util.Map;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Alejandro
 */
@Controller 
public class ReservarVuelosController {
    
    private JdbcTemplate jdbcTemplate;
    private VueloServicio vs;
    private CiudadServicio cs;
    private ReservaServicio rs;
    
    public ReservarVuelosController() 
    {
        Conexion con = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(con.conectar());
        this.vs = new VueloServicio();
        this.cs = new CiudadServicio();
        this.rs = new ReservaServicio();
    }
    
    @RequestMapping(value = "consultarVuelos.htm", method = RequestMethod.GET)
    public ModelAndView consultarVuelos()
    {
        ModelAndView mav = new ModelAndView();

        mav.addObject("vuelos", vs.consultarVuelos());
        mav.addObject("ciudades", cs.listarCiudades());
        mav.setViewName("template/contenido/consultar_vuelos");
        return mav;
    }
    
    @RequestMapping(value = "consultarVuelos.htm", method = RequestMethod.POST)
    public ModelAndView filtrarVuelos(@RequestParam("ciudad_origen")  String ciudadOrigen,
                                      @RequestParam("ciudad_destino")  String ciudadDestino
                                     )
    {        
        ModelAndView mav = new ModelAndView();

        mav.addObject("vuelos", vs.filtrarVuelos(ciudadOrigen, ciudadDestino));
        mav.addObject("ciudades", cs.listarCiudades());
        mav.setViewName("template/contenido/consultar_vuelos");
        return mav;
    }
    
    @RequestMapping(value = "consultarReservasUsuario.htm", method = RequestMethod.GET)
    public ModelAndView consultarReservasUsuario()
    {
        ModelAndView mav = new ModelAndView();

        mav.setViewName("template/contenido/consultar_reservas");
        return mav;
    }
    
    @RequestMapping(value = "consultarReservasUsuario.htm", method = RequestMethod.POST)
    public ModelAndView filtrarReservasUsuario(
                                                @RequestParam("nro_documento")  String nroDocumento
                                              )
    {
        ModelAndView mav = new ModelAndView();

        mav.addObject("reservasUsuario", rs.consultarReservasUsuario(nroDocumento));
        mav.setViewName("template/contenido/consultar_reservas");
        return mav;
    }
    
    @RequestMapping(value = "reservarVuelo.htm", method = RequestMethod.GET)
    public ModelAndView reservarVuelo()
    {
        ModelAndView mav = new ModelAndView();

        mav.addObject("vuelos", vs.consultarVuelos());
        mav.setViewName("template/contenido/reservar_vuelo");
        return mav;
    }
    
    @RequestMapping(value = "reservarVuelo.htm", method = RequestMethod.POST)
    public ModelAndView registrarReservarVuelo( @RequestParam("nro_documento") String nroDocumento,
                                                @RequestParam("nombres") String nombres,
                                                @RequestParam("apellidos") String apellidos,
                                                @RequestParam("edad") String edad,
                                                @RequestParam("vuelo") Integer vuelo
                                               )
    {
        ModelAndView mav = new ModelAndView();
        
        Usuario u = new Usuario(nroDocumento, nombres, apellidos, Integer.parseInt(edad));
        Vuelo v   = new Vuelo(vuelo);
        
        Reservacion r = new Reservacion();
        r.setUsuario(u);    
        r.setVueloSalida(v);
        
        rs.registrarReservaUsuario(r);

        mav.addObject("exito", 1);
        mav.setViewName("template/contenido/reservar_vuelo");
        return mav;
    }
    
    @RequestMapping(value = "listarVuelos", method = RequestMethod.POST)
    public @ResponseBody String listarVuelos(Model model, 
                                             @RequestParam("ciudad_origen")  String ciudadOrigen,
                                             @RequestParam("ciudad_destino")  String ciudadDestino
                                            ) 
    {
        List listaCiudades = cs.listarCiudades();
        int longitud       = listaCiudades.size();
        String vuelos      = "";

        for (int i = 0; i < longitud; i++) {
            System.out.println(listaCiudades.get(i));
            //vuelos +=
        }
                
        return vuelos;
    }
           
}
